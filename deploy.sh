#!/bin/bash

current_dir=`pwd`
docker_deploy_repo="$HOME/docker-deploy"
docker_compose_release_verion="1.23.2" # https://github.com/docker/compose/releases

# Check that script is running as root
# https://stackoverflow.com/a/18216122
if [ "$EUID" -ne 0 ]
    then echo "Please run as root."
    exit
fi

# Install docker-deploy
command -v git >/dev/null 2>&1 || {
    printf "Error: git is not installed\n"
    exit 1
}
env git clone --depth=1 https://gitlab.com/snaphost/docker-deploy.git "$docker_deploy_repo" || {
    printf "Error: git clone of docker-deploy repo failed\n"
    exit 1
}

# Install docker-ce
# apt-get update -y
# apt-get install apt-transport-https ca-certificates curl software-properties-common -y
# curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
# add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
# apt-get update -y
# apt-get install docker-ce -y

# Install docker-compose
# curl -L "https://github.com/docker/compose/releases/download/$docker_compose_release_version/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
# chmod +x /usr/local/bin/docker-compose
# docker-compose --version # logging
snap install docker

# Get deployment type from command-line argument
deploy_type="$1"
if [[ -n "$deploy_type" ]]; then
    echo "Deployment type is: $deploy_type"
else
    echo "No deployment type was specified."
fi

if (-d "scripts/$deploy_type"); then
    # directory exists, $deploy_type is valid
    command_to_run="docker-compose -f $docker_deploy_repo/scripts/$deploy_type/docker-compose.yml up -d"
    echo "$command_to_run"
    eval $command_to_run
else
    echo "ERROR!!! That deploy type argument is not valid."
fi

# case $deploy_type in
#     minecraft-vanilla)
#         command_to_run="docker-compose -f $docker_deploy_repo/minecraft-vanilla/docker-compose.yml up -d"
#         echo "$command_to_run"
#         eval $command_to_run
#         ;;
#     minecraft-forge)
#         command_to_run="docker-compose -f $docker_deploy_repo/minecraft-forge/docker-compose.yml up -d"
#         echo "$command_to_run"
#         eval $command_to_run
#         ;;
#     rocketchat)
#         command_to_run="docker-compose -f $docker_deploy_repo/rocketchat/docker-compose.yml up -d"
#         echo "$command_to_run"
#         eval $command_to_run
#         ;;
#     wordpress)
#         command_to_run="docker-compose -f $docker_deploy_repo/wordpress/docker-compose.yml up -d"
#         echo "$command_to_run"
#         eval $command_to_run
#         ;;
#     *)
#         echo "ERROR!!! That deploy type argument is not valid."
#         ;;
# esac
