# docker-deploy

A better way to launch apps.

## Getting started

### Basic installation

```
curl -s https://gitlab.com/snaphost/docker-deploy/raw/master/deploy.sh | bash -s <deploy_type>
```
